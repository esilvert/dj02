class Bullet < Ducky::KinematicBody
  attr_accessor :speed, :lifetime, :damage

  DEFAULT_SPEED = 8
  DEFAULT_LIFETIME = 0.15
  DEFAULT_DAMAGE = 10

  def initialize(position:,
                 direction:,
                 speed: DEFAULT_SPEED ,
                 lifetime: DEFAULT_LIFETIME,
                 damage: DEFAULT_DAMAGE,
                 name: "Bullet##{hash}"
                )
    super(collision_layer: :bullet, name: name, position: position)

    @damage = damage
    @speed = speed
    @direction = direction
    @lifetime = lifetime
    @life_duration = 0
    @base_velocity = @direction.normalized * speed
    @collider = nil
    @base_damage = damage

    @sprite = add_child(
      Ducky::Sprite.new(
        position: Vector2.zero,
        width: 40,
        height: 60,
        name: name,
        asset: 'misc/explosion-2.png'
      )
    )

    @shape = add_shape(
      Ducky::RectangleShape.new(
        position: Vector2.zero,
        size: Vector2.new(64, 64)
      )
    )

    on_collision_with(:enemy, &method(:on_enemy_collision))
  end

  def update(_args)
    velocity = @collider ? @collider.velocity : @base_velocity
    @life_duration += Ducky::DELTA_TIME if @collider.nil?
    @local_position += velocity

    if @life_duration > @lifetime
      queue_free!
    end

    # scale with damage
    scale = Math.log(@damage) / Math.log(2) rescue 0
    @sprite.width = 40 * scale
    @sprite.height = 60 * scale
    #@shape.size = Vector2.one * scale * 64
  end

  private

  def on_enemy_collision(collision)
    @collider = collision[:enemy]

    delta = 1 + @damage / 20

    @collider.suffer(delta)
    @damage -= delta

    queue_free! if @damage.zero?

    @collider = nil if @collider.dead
  end
end
