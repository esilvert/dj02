class Modulate < Ducky::Sprite
  attr_accessor :alpha

  def initialize(asset:)
    @alpha = 0
    super(
        position: Vector2.zero,
        width: 100.vw,
        height: 100.vh,
        asset: asset,
        modulate: Color.new(0, 0, 0, @alpha),
        blend_mode: Ducky::Sprite::Blending::ALPHA
    )
  end


  def to_dr
    super.merge({a: @alpha})
  end

end
