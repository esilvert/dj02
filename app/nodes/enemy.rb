class Enemy < Ducky::KinematicBody
  attr_reader :health, :velocity

  def initialize(position:, health: 1, name: "Enemy##{hash}", target:)
    super(collision_layer: :enemy, name: name, position: position)

    @health = health
    @target = target

    @scale = (@health / 4.0).clamp(1, 100)
    @asset_index = 0


    speed = 1
    direction = target - self.position
    @base_velocity = direction.normalized * speed

    @sprite = add_child(
      Ducky::Sprite.new(
        position: Vector2.zero,
        width: 40 * @scale,
        height: 60 * @scale,
        name: name,
        asset: 'misc/dragon-0.png'
      )
    )

    @health_label = add_child(
      Ducky::Label.new(
        position: Vector2.new(15 * @scale, - 10),
        text: @health.to_i.to_s,
        color: Color.red,
        size: 7
      )
    )

    add_shape(
      Ducky::RectangleShape.new(
        position: Vector2.right * 10,
        size: Vector2.new(30 * @scale, 120 * @scale)
      )
    )
  end

  def suffer(damage)
    @health = [@health - damage, 0].max

    queue_free! if @health.zero?
  end

  def update(args)
    @velocity = @base_velocity * delta_time
    @sprite.flip_horizontally = @velocity.x.negative?

    @local_position += @velocity

    @health_label.text = @health.to_i.to_s

    if args.state.tick_count % 30 == 0
      @asset_index = (@asset_index + 1) % 6
      @sprite.asset = asset_index(@asset_index)
    end
  end

  private

  def asset_index(idx)
    "misc/dragon-#{idx}.png"
  end

end
