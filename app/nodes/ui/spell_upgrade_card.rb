class SpellUpgradeCard < Ducky::Node2D
  def initialize(position:, bullet_pool:, name: "SpellUpgradeCard##{hash}")
    super(position: position, name: name)

    width = 300.0
    height = 500.0

    @bullet_pool = bullet_pool

    @panel = add_child(
      Ducky::Sprite.new(
        position: Vector2.zero,
        width: width,
        height: height,
        asset: 'ui/card.png'
      )
    )

    @image = add_child(
      Ducky::Sprite.new(
        position: Vector2.new(width / 4, height - 200),
        width: width / 4 * 2,
        height: width / 4 * 2,
        asset: 'ui/book.png'
      )
    )

    @description = add_child(
      Ducky::Label.new(
        text: description_for(@upgrade),
        position: Vector2.new(width / 4 - 40, 200 ),
        align: Ducky::Label::TextAlign::LEFT,
        size: 0
      )
    )
    # @description.fit_text_into(width / 2, 150, allow_growth: true)

    @cta = add_child(
      Ducky::Button.new(
        position: Vector2.new(width / 4, 50),
        width: width / 2,
        height: 80,
        text: 'Upgrade !',
        center: false,
        asset: 'ui/button.png'
      )
    ).on_clicked{ @click_callback&.call(@upgrade) }
  end

  def refresh(upgrade)
    @upgrade = upgrade
    @description.text = description_for(@upgrade)
  end

  def on_clicked(&block)
    @click_callback = block
    self
  end

  def description_for(upgrade)
    if upgrade.is_a?(SpellUpgradeSpeed)
      'Speed : goes brrrr'
    elsif upgrade.is_a?(SpellUpgradeSlow)
      'Slow : goes slower'
    elsif upgrade.is_a?(SpellUpgradeLifetime)
      'Lifetime : travels longer'
    elsif upgrade.is_a?(SpellUpgradeDamage)
      'Damage : hits more'
    end
  end
end
