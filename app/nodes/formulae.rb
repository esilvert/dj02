class Formulae < Ducky::Node2D

  def initialize(
        position: Vector2.zero,
        color: Color.white,
        bullet_pool:,
        modulate:,
        name: "Formulae##{hash}"
      )

    super(position: position, name: name)

    @bullet_pool = bullet_pool
    @modulate = modulate

    @label = add_child(
      Ducky::Label.new(
        position: Vector2.zero,
        color: color,
        size: 5,
        text: 'WIP'
      )
    )

    offset_x_to_center = -@label.text_size.x / 2 + 5

    @progress_label = Ducky::Label.new(
      position: Vector2.zero,
      color: Color.red,
      size: 5,
      text: 'WIP'
    )
    @label.position.x += offset_x_to_center

    add_child(@progress_label)

    new_spell
  end

  def update(args)
    if @current_char_index == @text.size
      @succeeded = true
      @modulate.alpha = 0
      play_spell_sound
      return
    end

    update_input(args)
    update_effects(args)
  end

  def succeeded?
    @succeeded
  end

  def new_spell
    @text = FormulaeFactory.pick(@bullet_pool)
    @label.text = @text
    @progress_label.text = ''
    @current_char_index = 0
    @succeeded = false
  end

  private

  def input_key_for(letter)
    normalized_letter = letter.downcase.to_sym

    case letter
    when ' ' then return :space
    when '!' then return :exclamation_point
    when '0' then return :zero
    when '1' then return :one
    when '2' then return :two
    when '3' then return :three
    when '4' then return :four
    when '5' then return :five
    when '6' then return :six
    when '7' then return :seven
    when '8' then return :eight
    when '9' then return :nine
    end

    normalized_letter
  end

  def update_input(args)
    letter = @text[@current_char_index]
    is_capital = letter.downcase != letter
    key = input_key_for(letter)

    if(
      Ducky::Input.input_just_pressed?(key) &&
      (
        (!is_capital && Ducky::Input.input_released?(:shift)) ||
        (is_capital && Ducky::Input.input_pressed?(:shift))
      )
    )
      play_tick_sound(@current_char_index)
      @progress_label.text = ''
      @text.size.times do |i|
        if i <= @current_char_index
          @progress_label.text += @label.text[i]
        else
          @progress_label.text += ' '
        end
      end
      @current_char_index = (@current_char_index + 1).clamp(0, @text.size)
    end
  end

  def update_effects(args)
    return if @text.size <= 3

    # Text Size
    size_progress = (@current_char_index) / @text.size.to_f

    size = lerp(5, 5 + @text.size * 2, size_progress) + rand * (@text.size - 3) * (size_progress)
    @label.size = size
    @progress_label.size = size

    # Modulate
    modulate_treshold = 8
    return if @text.size < modulate_treshold
    modulate_progress = (@current_char_index - modulate_treshold) / [@text.size - modulate_treshold, 15].max.to_f
    @modulate.alpha = lerp(0, 200, modulate_progress)
  end

  def play_spell_sound
    $gtk.args.audio[:spell] = {
    input: "sounds/spells/spell_#{[1,2,3].sample}.wav",  # Filename
    x: 0.0, y: 0.0, z: 0.0,   # Relative position to the listener, x, y, z from -1.0 to 1.0
    gain: 0.3,                # Volume (0.0 to 1.0)
    pitch: 1.0,               # Pitch of the sound (1.0 = original pitch)
    paused: false,            # Set to true to pause the sound at the current playback position
    looping: false,           # Set to true to loop the sound/music until you stop it
  }
  end

  def play_tick_sound(n)
    gain = ((n-3) * 0.05 ).clamp(0.1, 1)

    $gtk.args.audio["spell_tick_#{rand(1e9)}"] = {
      input: 'sounds/spells/tick.wav',  # Filename
      x: 0.0, y: 0.0, z: 0.0,   # Relative position to the listener, x, y, z from -1.0 to 1.0
      gain: gain,                # Volume (0.0 to 1.0)
      pitch: 1.0,               # Pitch of the sound (1.0 = original pitch)
      paused: false,            # Set to true to pause the sound at the current playback position
      looping: false,           # Set to true to loop the sound/music until you stop it
    }
  end
end
