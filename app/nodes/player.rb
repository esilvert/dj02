class Player < Ducky::KinematicBody
  STATES = [
    :idle,
    :moving,
    :channelling
  ]

  attr_reader :fsm

  def initialize(position:, bullet_pool:, modulate:, name: "Player##{hash}")
    super(collision_layer: :player, name: name, position: position)

    @fsm = Ducky::FSM.new(root: self, states: STATES)
    @bullet_pool = bullet_pool
    @direction = Vector2.right

    @sprite = add_child(
      Ducky::Sprite.new(
        position: Vector2.zero,
        width: 120,
        height: 240,
        name: name,
        asset: 'player/idle.png'
      )
    )

    @formulae = add_child(
      Formulae.new(
        position: Vector2.new(0, 300),
        color: Color.white,
        bullet_pool: @bullet_pool,
        modulate: modulate
      )
    )
    @formulae.disable!

    add_shape(
      Ducky::RectangleShape.new(
        position: Vector2.zero,
        size: Vector2.new(120, 240)
      )
    )

    Ducky::Input.register_action(:player_left, :left)
    Ducky::Input.register_action(:player_right, :right)
    Ducky::Input.register_action(:player_spell, :space)
  end

  def update(_args)
    @sprite.flip_horizontally = @direction.x == -1

    @fsm.update
  end

  def state_idle
    if Ducky::Input.action_pressed?(:player_right) || Ducky::Input.action_pressed?(:player_left)
      @fsm.next_state = :moving
    end

    if Ducky::Input.action_just_pressed?(:player_spell)
      @fsm.next_state = :channelling
    end
  end

  def state_moving
    speed = 4

    velocity = Vector2.zero
    velocity.x += 1 if Ducky::Input.action_pressed?(:player_right)
    velocity.x -= 1 if Ducky::Input.action_pressed?(:player_left)

    @direction = Vector2.new(velocity.x, 0).normalized unless velocity.x.zero?

    @local_position += velocity.normalized * speed

    if velocity == Vector2.zero
      @fsm.next_state = :idle
    end
  end

  def on_state_channelling
    @formulae.new_spell
    @formulae.enable!
    @sprite.asset = 'player/channelling.png'
  end

  def on_state_idle
    @sprite.asset = 'player/idle.png'
  end

  def on_state_moving
    @sprite.asset = 'player/idle.png'
  end

  def state_channelling
    if @formulae.succeeded?
      @formulae.disable!

      @bullet_pool.shoot(
        position: position + Vector2.up * 100 + (Vector2.right * 120) * @direction.x,
        direction: @direction
      )
      @fsm.next_state = :idle
    end
  end
end
