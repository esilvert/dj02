class WaveManager < Ducky::Node
  attr_accessor :player_ready

  def initialize(enemy_pool:, name: "WaveManager##{hash}}")
    super(name: name)

    @enemy_pool = enemy_pool
    @wave_enemies = []

    @wave_duration = 0
    @wave_index = 0
    @player_ready = false
    @wave_finished = true
    @remaining_enemies_to_spawn = 0
  end

  def update(args)
    @wave_duration += Ducky::DELTA_TIME

    if @remaining_enemies_to_spawn.nonzero?
      if args.state.tick_count % 120 == 0
        this_tick_count = (rand(@wave_index / 2) + 1)
        @remaining_enemies_to_spawn -= this_tick_count
        @remaining_enemies_to_spawn = 0 if @remaining_enemies_to_spawn.negative?
        log "Spawn #{this_tick_count} enemies"
        this_tick_count.times do
          @wave_enemies << @enemy_pool.spawn_land(wave: @wave_index)
        end
      end
    end

    if @wave_finished == false && @wave_enemies.all?(&:dead) && @remaining_enemies_to_spawn.zero?
      @wave_finished = true
      @wave_ended_callback.call(@wave_index)
    end

    start_next_wave if should_start_next_wave?
  end

  def on_wave_ended(&block)
    @wave_ended_callback = block
    self
  end

  private

  def should_start_next_wave?
    enemies_down = @wave_enemies.all?(&:dead) || @wave_enemies.empty?

    @player_ready && enemies_down
  end

  def start_next_wave
    @wave_index += 1
    @wave_enemies = []
    @wave_duration = 0
    @player_ready = false
    @wave_finished = false
    @remaining_enemies_to_spawn = growth(@wave_index)
  end

  def growth(n)
    n
  end


  def fibonnacci(n)
    @fib ||= Hash.new {|hash, key| hash[key] = key < 2 ? key : hash[key-1] + hash[key-2] }

    @fib[n]
  end
end
