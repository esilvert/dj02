class Objective < Ducky::StaticBody
  def initialize(position:, collision_layer: :objective, name: "Objective##{hash}")
    super(position: position, collision_layer: collision_layer, name: name)

    @lost = false

    @sprite = add_child(
      Ducky::Sprite.new(
        position: self.position * -1,
        width: 100.vw,
        height: 100.vh,
        asset: 'level/objective.png'
      )
    )

    add_child(
      Ducky::Sprite.new(
        position: Vector2.zero,
        width: 100,
        height: 150,
        asset: 'square/red.png'
      )
    )

    add_shape(
      Ducky::RectangleShape.new(
        position: Vector2.zero,
        size: Vector2.new(100, 150)
      )
    )

    on_collision_with(:enemy, &method(:on_enemy_collision))

    $gtk.args.state.objective.position = position
  end

  def lost?
    @lost
  end

  private

  def on_enemy_collision(collision)
    @lost = true
  end
end
