class EnemyPool < Ducky::Node2D
  attr_reader :enemies

  def initialize(position:, name: "EnemyPool##{hash}")
    super(position: position, name: name)

    @enemies = []
  end

  def spawn_land(wave:)
    x = [-rand(50 + wave ** 2).vw, 100.vw + rand(50 + wave ** 2).vw].sample
    position = Vector2.new(x, 100)

    add_child(
      Enemy.new(
        position: position,
        target: objective_position,
        health: 2 + (wave ** 2).to_i
      )
    ).tap do |enemy|
      @enemies << enemy
    end
  end

  def draw(_args); end

  private

  def objective_position
    $gtk.args.state.objective.position
  end
end
