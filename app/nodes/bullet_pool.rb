class BulletPool < Ducky::Node2D
  attr_reader :upgrades

  def initialize(position:, name: "BulletPool##{hash}")
    super(position: position, name: name)

    @bullets = []
    @upgrades = []
  end

  def shoot(position:, direction:)
    bullet = new_bullet(position: position, direction: direction)

    upgrade_type_count = Hash.new{ |new_h, new_type| new_h[new_type] = 0}

    @upgrades.each do |upgrade|
      upgrade_idx = upgrade_type_count[upgrade.class] += 1

      upgrade.upgrade(bullet, index: upgrade_idx, pool: self)
    end

    @bullets << add_child(bullet)
  end

  def new_bullet(position:, direction:)
    Bullet.new(position: position, direction: direction)
  end

  def add_upgrade(upgrade)
    log "Adds an upgrade of type #{upgrade.class}"

    @upgrades << upgrade
  end

  def draw(_args); end
end
