class Vector2 < Ducky::Vector2; end
class Color < Ducky::Color; end

class Game < Ducky::Game
  def configuration
    add_scene :main_menu, MainMenu
    add_scene :tutorial, DefendTutorial
    add_scene :defend, Defend
  end
end
