class TestFormulae < Ducky::Scene
  def initialize
    super
    @background_color = Color.new(100, 100, 100)

    # Insert your nodes here
    @formulae = register_label(
      Formulae.new(
        text: 'Lorem Ipsum',
        position: Vector2.new('50%', '50%'),
        color: Color.white,
        size: 5
      )
    )

    # Scene navigation
    @next_scene = MainMenu # This is mandatory, replace with your next_scene class (Level)
  end

  def complete?
    @formulae.succeeded?
  end

  def must_exit?
    false
  end
end
