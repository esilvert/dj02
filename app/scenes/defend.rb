class Defend < Ducky::Scene
  def initialize
    super

    register_static_nodes
    register_objective
    register_enemies
    register_ui
    register_effects
    register_player

    show_upgrades

    # Scene navigation
    @next_scene = MainMenu # This is mandatory, replace with your next_scene class (Level)

    # $gtk.args.outputs.sounds << 'musics/theme.ogg'
  end

  def complete?
    @objective.lost?
  end

  def must_exit?
    false
  end

  def update(args)
    if @player.fsm.current_state == :channelling
      pause_enemies
    else
      resume_enemies
    end
  end

  private

  def register_static_nodes
    @background_color = Color.duck_blue

    @ground = register_sprite(
      Ducky::Sprite.new(
        position: Vector2.zero,
        width: 100.vw,
        height: 150,
        asset: 'level/ground.png'
      ),
      static: true
    )
  end

  def register_objective
    @objective = register_sprite(
      Objective.new(
        position: Vector2.new(45.vw - 50, 100)
      )
    )
  end

  def register_enemies
    @enemy_pool = register_node(
      EnemyPool.new(position: Vector2.zero)
    )

    @wave_manager = register_node(
      WaveManager.new(enemy_pool: @enemy_pool)
    ).on_wave_ended(&method(:on_wave_ended))
  end

  def register_player
    @bullet_pool = register_node(
      BulletPool.new(position: Vector2.zero)
    )

    @player = register_node(
      Player.new(
        position: Vector2.new(50.vw - 100, 50),
        bullet_pool: @bullet_pool,
        modulate: @modulate
      )
    )
  end

  def register_ui
    @controls = []
    @controls << register_label(
      Ducky::Label.new(
        position: Vector2.new(50.vw, 65.vh),
        text: 'Press <Space> to cast a spell',
        color: Color.red,
        size: 7
      )
    )
    @controls.each(&:disable!)

    @cards = []
    3.times do |idx|
      @cards << register_node(
        SpellUpgradeCard.new(
          position: Vector2.new(7.vw + 30.vw * idx, 20.vh),
          bullet_pool: @bullet_pool
        )
      ) { |card| card.on_clicked(&method(:on_card_clicked)) }
    end

    @cards.each(&:disable!)
  end

  def register_effects
    @modulate = register_sprite(
      Modulate.new(asset: 'square/black.png')
    )
  end

  def show_upgrades
    @controls.each(&:disable!)
    upgrades = SpellUpgradePicker.pick(3)
    @cards.each.with_index { |card, idx| card.enable! ; card.refresh(upgrades[idx].new) }
    @player.disable!
  end

  def show_game
    @controls.each(&:enable!)
    @cards.each(&:disable!)
    @player.enable!
  end

  def on_card_clicked(upgrade)
    show_game
    @bullet_pool.add_upgrade(upgrade)
    @wave_manager.player_ready = true
  end

  def on_wave_ended(_wave_idx)
    show_upgrades
  end

  def pause_enemies
    @enemy_pool.enemies.each { |enemy| enemy.time_scale = 0.2 }
  end

  def resume_enemies
    @enemy_pool.enemies.each { |enemy| enemy.time_scale = 1.0 }
  end
end

Ducky.reload
