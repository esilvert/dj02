class DefendTutorial < Defend
  def initialize
    super
    show_game
    @controls.each(&:disable!)

    @next_scene = Defend # This is mandatory, replace with your next_scene class (Level)

    @tutorial_step = 0

    @wave_ended = false
    @chose_upgrade = false
    @player_clicked_play = false

    @steps = [
      { text: "Greetings Summoner !"},
      { text: "Welcome to your castle."},
      { text: "Sorry to disppoint you, but you're under attack"},
      { text: "Please defend the realm against the many enemies !"},
      { text: "Here, kill this one by casting a spell", on_show: :first_wave, condition: -> {@wave_ended}},
      { text: "You learn a lot killing enemies", on_show: :hide_controls},
      { text: "At the end of each wave, you can become a better wizzard"},
      { text: "To do so, pick one of the upgrade you'll have proposed", on_show: :show_upgrades, condition: ->{@chose_upgrade} },
      { text: "Every upgrade leans you how to pronounce your spell", on_show: :hide_upgrades},
      { text: "Speed makes you prononce more words with fewer letters"},
      { text: "Lifetime makes you insist on voyels. You get the idea. "},
      { text: "A new wave is here, save uUuuUUuUus !", on_show: :new_wave, condition: ->{@wave_ended}  },
      { text: "Phew, we're saved \\o/. Now... Let the game begin !", on_show: :show_play},
    ]

    @play_button = register_node(
      Ducky::Button.new(
        position: Vector2.new(45.vw, 80.vh),
        width: 10.vw,
        height: 5.vh,
        text: 'Play'
      ).on_clicked(&method(:start_game))
    )
    @play_button.disable!

    @next_step = register_node(
      Ducky::Button.new(
        position: Vector2.new(45.vw, 80.vh),
        width: 10.vw,
        height: 5.vh,
        text: 'Next'
      ).on_clicked(&method(:next_step))
    )

    @text = register_label(
      Ducky::Label.new(
        position: Vector2.new(50.vw, 95.vh),
        size: 10,
        text: 'WIP',
        color: Color.red
      )
    )

    next_step
  end

  def next_step
    @tutorial_step += 1
    @next_step.disable!
    @wave_ended = false

    step = @steps[@tutorial_step]
    send(step[:on_show]) if step.key?(:on_show)
    @text.text = step[:text]
  end

  def complete?
    @player_clicked_play
  end

  def update(args)
    step = @steps[@tutorial_step]

    if step[:condition].nil? || step[:condition].call
      next_step if step[:condition]&.call
      @next_step.enable! if step[:condition].nil? && step != @steps[-1]
      step = @steps[@tutorial_step]
    end


    if @player.fsm.current_state == :channelling
      pause_enemies
    else
      resume_enemies
    end
  end

  private

  def show_controls
    @controls.each(&:enable!)
  end

  def hide_controls
    @controls.each(&:disable!)
  end

  def new_wave
    show_game
    @bullet_pool.add_upgrade(@upgrade)
    @wave_manager.player_ready = true
  end

  def show_play
    @next_step.disable!
    @controls.each(&:disable!)
    @play_button.enable!
  end


  def on_card_clicked(upgrade)
    @chose_upgrade = true
    @upgrade = upgrade
  end

  def on_wave_ended(_wave_idx)
    @wave_ended = true
    log "WAVE ENDED !!!!!!!!!!!!!!"
  end

  def start_game
    @player_clicked_play = true
  end

  def first_wave
    show_controls
    @wave_manager.player_ready = true
  end

  def hide_upgrades
    @controls.each(&:disable!)
    @cards.each(&:disable!)
  end

end

Ducky.reload
