class FormulaeFactory
  WORDS = %w[lorem
             ipsum
             dolor
             sit
             amet
             consectetur
             adipiscing
             elit
             nullam
             accumsan
             mollis
             quam
             sit
             amet
             hendrerit
             elit
             varius
             id
             nulla
             vehicula].freeze

  def self.max_length
    WORDS.max_by(&:length).size
  end

  def self.pick(pool)
    new_formulae = {
      word_count: 1,
      word_min_size: 5,
      word_max_size: 8,
      capital_count: 0,
      double_count: 0,
    }

    pool.upgrades.each do |upgrade|
      upgrade.upgrade_formulae(new_formulae)
    end

    generate_with(new_formulae)
  end

  def self.generate_with(config)
    new_formulae = []
    config[:word_count].times { new_formulae << pick_word(config) }

    config[:double_count].times do
      random_index = rand(new_formulae.size)
      word = new_formulae[random_index]

      voyel = %w[a e i o u].shuffle.detect{ |letter| word.include?(letter) }

      cursor = voyel ? word.index(voyel) : word[-1]

      letter = word[cursor]
      new_formulae[random_index] = [word[0...cursor].to_s, letter.to_s, word[cursor..-1].to_s].join
    end

    average_capital_per_word = config[:capital_count] / config[:word_count]
    config[:capital_count].times do
      random_index = rand(new_formulae.size)
      word = new_formulae[random_index]

      if word.size < average_capital_per_word
        new_formulae[random_index] = word.upcase
      else
        start = word[0...average_capital_per_word].upcase
        ending = word[average_capital_per_word..-1]
        new_formulae[random_index] = [start, ending].join
      end
    end

    new_formulae.join(' ')
  end

  def self.pick_word(config)
    WORDS.select do |word|
      word.size >= config[:word_min_size] && word.size <= config[:word_max_size]
    end.sample
  end
end
