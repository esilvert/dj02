class SpellUpgrade
  def upgrade(bullet, pool:, index: 1)
    raise NotImplementedError, 'SpellUpgrade#upgrade must be override'
  end

  def upgrade_formulae(formulae)
    raise NotImplementedError, 'SpellUpgrade#upgrade_formulae must be override'
  end
end

class SpellUpgradeSpeed < SpellUpgrade
  def upgrade(bullet, pool:, index: 1)
    bullet.speed *= 1.2
  end

  def upgrade_formulae(formulae)
    if formulae[:word_max_size] <= 2
      formulae[:word_count] += 1
      formulae[:word_max_size] += 5
    else
      formulae[:word_max_size] -= 1
    end
  end
end

class SpellUpgradeSlow < SpellUpgrade
  def upgrade(bullet, pool:, index: 1)
    bullet.speed /= 1.2
  end

  def upgrade_formulae(formulae)
    if formulae[:word_max_size] >= FormulaeFactory.max_length
      formulae[:word_max_size] -= 3
      formulae[:word_count] += 1
    else
      formulae[:word_max_size] += 1
    end
  end
end

class SpellUpgradeLifetime < SpellUpgrade
  def upgrade(bullet, pool:, index: 1)
    bullet.lifetime += 0.3
  end

  def upgrade_formulae(formulae)
    if formulae[:double_count] >= 4
      formulae[:word_count] += 1
      formulae[:double_count] -= 2
    else
      formulae[:double_count] += 1
    end
  end
end

class SpellUpgradeDamage < SpellUpgrade
  def upgrade(bullet, pool:, index: 1)
    bullet.damage *= 4
  end

  def upgrade_formulae(formulae)
    if formulae[:capital_count] >= formulae[:word_count] * formulae[:word_min_size]
      formulae[:word_count] += 1
      formulae[:capital_count] -= 5
    else
      formulae[:capital_count] += 1
    end
  end
end
