class SpellUpgradePicker
  UPGRADES = [
    SpellUpgradeSpeed,
    SpellUpgradeSlow,
    SpellUpgradeLifetime,
    SpellUpgradeDamage,
  ].freeze

  def self.pick(n)
    raise ArgumentError, "Can't pick #{n} unique upgrades in a list of #{UPGRADES.size} upgrades" if n > UPGRADES.size

    UPGRADES.dup.shuffle.take(n)
  end
end
