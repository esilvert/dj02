# Internal Dependencies
require 'app/ducky/ducky.rb'

def on_game_start
  $ducky.game_class = Game
end

def require_game_files
  require 'app/lib/spell_upgrades.rb'
  require 'app/lib/spell_upgrade_picker.rb'
  require 'app/lib/formulae_factory.rb'

  require 'app/nodes/formulae.rb'
  require 'app/nodes/objective.rb'
  require 'app/nodes/player.rb'
  require 'app/nodes/bullet_pool.rb'
  require 'app/nodes/bullet.rb'
  require 'app/nodes/enemy_pool.rb'
  require 'app/nodes/enemy.rb'
  require 'app/nodes/wave_manager.rb'
  require 'app/nodes/ui/spell_upgrade_card.rb'
  require 'app/nodes/modulate.rb'

  require 'app/scenes/main_menu.rb'
  require 'app/scenes/defend.rb'
  require 'app/scenes/defend_tutorial.rb'
  require 'app/game.rb'
end

def tick(args)
  $ducky ||= Ducky.configure

  $ducky.require_game_files_with(&method(:require_game_files))
  $ducky.start_game_with(&method(:on_game_start))

  $ducky.tick(args)
end
